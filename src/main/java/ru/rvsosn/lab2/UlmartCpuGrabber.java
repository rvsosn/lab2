package ru.rvsosn.lab2;

import one.util.streamex.StreamEx;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.stream.Stream;

public class UlmartCpuGrabber {
    private static final Logger logger = LoggerFactory.getLogger(UlmartCpuGrabber.class);
    private static final String SITE_ADDR = "http://www.ulmart.ru/catalog/cpu";


    public Stream<Entry> loadCpuPrices() {
        logger.info("Loading prices from {}", SITE_ADDR);
        try {
            Connection.Response execute = Jsoup.connect(SITE_ADDR).execute();

            Elements selectedElements = execute
                    .parse()
                    .body()
                    .select("div#catalogGoodsBlock section");

            return StreamEx.of(selectedElements)
                    .map(el -> {
                        Element titleEl = el.select(".b-product__title").get(0);
                        Element priceEl = el.select(".b-product__price span.b-price__num").get(0);
                        return new Entry(titleEl.text(), priceEl.text());
                    })
                    .peek(s -> logger.debug("Parsed entry {}", s));
        } catch (IOException ex) {
            logger.warn("We have some troubles with getting prices", ex);
            throw new RuntimeException(ex);
        }
    }

    public static class Entry {
        private String title;
        private String price;

        private Entry(String title, String price) {
            this.title = title;
            this.price = price;
        }

        public String getTitle() {
            return title;
        }

        public String getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return String.format("%s (%s)", title, price);
        }
    }
}