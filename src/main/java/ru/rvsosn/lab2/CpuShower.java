package ru.rvsosn.lab2;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.concurrent.ForkJoinPool;

public class CpuShower {
    private static final Logger logger = LoggerFactory.getLogger(CpuShower.class);
    private static final String LINE_FORMAT = "<div>%s<br /><a>(%s)</a></div>";
    private static final int SERVER_ERROR_CODE = 503;
    private static final int OK_CODE = 200;
    private static final int ZERO_CONTENT_LENGTH = 0;
    private static final int SERVER_PARALLELISM = Runtime.getRuntime().availableProcessors();

    private final InetSocketAddress addr;
    private final UlmartCpuGrabber grabber;

    public CpuShower(UlmartCpuGrabber grabber, InetSocketAddress addr) {
        this.grabber = grabber;
        this.addr = addr;
    }

    public void startAndJoin() {
        try {
            HttpServer server = HttpServer.create();
            server.bind(addr, 0);
            server.createContext("/", constructHandler());
            server.setExecutor(new ForkJoinPool(SERVER_PARALLELISM));
            server.start();
        } catch (IOException e) {
            logger.error("Error with start server", e);
            throw new RuntimeException(e);
        }
    }

    private HttpHandler constructHandler() {
        return httpExchange -> {
            logger.info("Start handling request from {}", httpExchange.getRemoteAddress());

            StringBuilder resp = new StringBuilder();

            // Выставляем кодировку UTF-8
            httpExchange.getResponseHeaders().put("Content-Type", Collections.singletonList("text/html; charset=UTF-8"));

            try {
                grabber.loadCpuPrices()
                        .forEach(s -> resp.append(String.format(LINE_FORMAT, s.getTitle(), s.getPrice())));

                logger.info("Finish handling request from {}", httpExchange.getRemoteAddress());
            } catch (RuntimeException ex) {
                logger.warn("Cannot handle request", ex);

                httpExchange.sendResponseHeaders(SERVER_ERROR_CODE, ZERO_CONTENT_LENGTH);
                httpExchange.close();

                return;
            }

            byte[] respBytes = resp.toString().getBytes();
            httpExchange.sendResponseHeaders(OK_CODE, respBytes.length);

            OutputStream responseBody = httpExchange.getResponseBody();
            responseBody.write(respBytes);
            responseBody.close();
        };
    }
}
