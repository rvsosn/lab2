package ru.rvsosn.lab2;

import java.net.InetSocketAddress;

public class Runner {

    public static void main(String[] args) {
        CpuShower shower = new CpuShower(
                new UlmartCpuGrabber(),
                new InetSocketAddress(6060));
        shower.startAndJoin();
    }
}